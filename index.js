const express = require("express");
const app = express();

app.get("/", (req, res) => {
    res.send({
        status: 200,
        data: {},
        message: "Welcome to the demo app !"
    })
});

const port = process.env.PORT || 3000;
app.listen(port, () => {
    console.log("Server Up And Running !", port);
})